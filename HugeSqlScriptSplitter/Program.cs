﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HugeSqlScriptSplitter
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = @"D:\database\fzg\script";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(path + ".sql"))
            {
                int fileNumber = 0;
                string[] headerLines = null;

                const string separator = "GO";
                while (!sr.EndOfStream)
                {
                    var header = new List<string>();
                    int count = 0;

                    var destinationFilePath = path + "-" + ++fileNumber + ".sql";
                    Console.WriteLine($"Writing {destinationFilePath}...");
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(new FileStream(destinationFilePath, FileMode.Create), Encoding.Unicode))
                    {
                        sw.AutoFlush = true;

                        if (fileNumber > 1 && headerLines != null)
                        {
                            foreach (var headerLine in headerLines)
                                sw.WriteLine(headerLine);
                        }

                        while (!sr.EndOfStream)
                        {
                            var line = sr.ReadLine();
                            sw.WriteLine(line);

                            if (fileNumber == 1 && headerLines == null)
                            {
                                header.Add(line);
                                if (line == separator)
                                    headerLines = header.ToArray();
                            }

                            if (++count > 20000 && sw.BaseStream.Length > 1000000000 && line == separator)
                                break;
                        }
                    }
                }
            }
        }
    }
}
